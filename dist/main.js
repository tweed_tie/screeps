const RoomManager = require('components_roomManager');

const loop = () => {
    // Manage all rooms
    for (const roomName in Game.rooms) {
        const room = Game.rooms[roomName];
        const roomManager = new RoomManager(room);
        roomManager.run();
    }

    // Clear dead creeps from the memory
    for (const creepName in Memory.creeps) {
        if (!Game.creeps[creepName]) {
            delete Memory.creeps[creepName];
        }
    }
};

module.exports.loop = loop;
