const Creep = require('components_creeps_creep');

const Upgrader = require('components_creeps_roles_upgrader');

class Builder extends Creep {
    constructor(creep) {
        super(creep);
    }

    run() {
        this.updateWorkingStatus();

        if (this.memory.working) {
            this.work();
        } else {
            const target = this.creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);

            if (target) {
                if (this.creep.build(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    this.creep.moveTo(target);
                }
            } else {
                const upgrader = new Upgrader(this.creep);
                upgrader.run();
            }
        }
    }
}

module.exports = Builder;
