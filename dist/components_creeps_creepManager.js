const Creep = require('components_creeps_creep');

const roles = {
    harvester: require('components_creeps_roles_harvester'),
    upgrader: require('components_creeps_roles_upgrader'),
    builder: require('components_creeps_roles_builder'),
    repairer: require('components_creeps_roles_repairer'),
    miner: require('components_creeps_roles_miner'),
    distanceHarvester: require('components_creeps_roles_distanceHarvester')
};

class CreepManager {
    constructor(room, creeps) {
        this.room = room;
        this.creeps = creeps;

        // Create a default desiredCreeps object if it doesn't exist yet
        if (!room.memory.desiredCreeps) {
            room.memory.desiredCreeps = {
                'harvester': 3,
                'upgrader': 3,
                'builder': 3,
                'repairer': 1
            };
        }
    }

    run() {
        this._spawnNeededCreeps();
        this._runCreepRoles();
    }

    _spawnNeededCreeps() {
        const roomPopulation = {};
        const desiredCreeps = this.room.memory.desiredCreeps;

        // Count creeps per role
        for (const role in roles) {
            roomPopulation[role] = _.filter(this.creeps, (creep) => creep.memory.role == role).length;
        }

        // Spawn a harvester if there aren't any harvesters nor miners left
        if (!roomPopulation.harvester && !roomPopulation.miner) {
            const spawns = this.room.find(FIND_MY_STRUCTURES, {
                filter: structure => structure.structureType === STRUCTURE_SPAWN && !structure.spawning
            });

            if (spawns[0]) {
                Creep.createCreep(spawns[0], `harvester-${Game.time}`, { role: 'harvester' }, [WORK, CARRY, MOVE]);
            }
        }

        // Spawn needed creeps
        for (const role in desiredCreeps) {
            if (roomPopulation[role] < desiredCreeps[role]) {
                const spawns = this.room.find(FIND_MY_STRUCTURES, {
                    filter: structure => structure.structureType === STRUCTURE_SPAWN && !structure.spawning
                });

                if (spawns[0]) {
                    Creep.createCreep(spawns[0], `${role}-${Game.time}`, { role }, Creep.createCreepBody(this.room.energyCapacityAvailable));
                    break;
                }
            }
        }

        // Spawn needed miners if there are sources with a container
        const sources = this.room.find(FIND_SOURCES);
        for (const source of sources) {
            if (!_.some(this.creeps, c => c.memory.role === 'miner'
                    && c.memory.sourceId === source.id)) {
                const containers = source.pos.findInRange(FIND_STRUCTURES, 1, {
                    filter: s => s.structureType === STRUCTURE_CONTAINER
                });

                if (containers) {
                    const spawns = this.room.find(FIND_MY_STRUCTURES, {
                        filter: structure => structure.structureType === STRUCTURE_SPAWN && !structure.spawning
                    });

                    Creep.createCreep(spawns[0], `miner-${Game.time}`, { role: 'miner', sourceId: source.id }, [WORK, WORK, WORK, WORK, WORK, MOVE]);
                }
            }
        }

        for (const room in this.room.memory.distanceHarvesting) {
            const numberOfHarvesters = _.filter(Game.creeps,
                c => c.memory.role === 'distanceHarvester'
                    && c.memory.targetRoom === room).length;

            if (numberOfHarvesters < this.room.memory.distanceHarvesting[room]) {
                const spawns = this.room.find(FIND_MY_STRUCTURES, {
                    filter: structure => structure.structureType === STRUCTURE_SPAWN && !structure.spawning
                });

                Creep.createCreep(spawns[0], `distanceHarvester-${Game.time}`, { role: 'distanceHarvester', targetRoom: room },
                [WORK, WORK, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY]);
            }
        }
    }

    _runCreepRoles() {
        for (const creep of this.creeps) {
            const c = new roles[creep.memory.role](creep);
            c.run();
        }
    }
}

module.exports = CreepManager;
