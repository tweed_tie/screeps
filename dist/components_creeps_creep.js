class Creep {
    constructor(creep) {
        this.creep = creep;

        this.allowedStorageUse = false;

        this.memory = creep.memory;
        this.carry = creep.carry;
        this.carryCapacity = creep.carryCapacity;
        this.room = creep.room;
    }

    // Update the working status of the creep (working === getting energy)
    updateWorkingStatus() {
        if (this.memory.working && this.carry[RESOURCE_ENERGY] === this.carryCapacity) {
            this.memory.working = false;
        } else if (!this.memory.working && this.carry[RESOURCE_ENERGY] === 0) {
            this.memory.working = true;
        }
    }

    // Take energy from either containers or sources if there are no containers
    work() {
        const containers = this.creep.room.find(FIND_STRUCTURES, {
            filter: s => s.structureType === STRUCTURE_CONTAINER
                        && s.store[RESOURCE_ENERGY] > 0
        });

        if (this.room.storage && this.room.storage.store[RESOURCE_ENERGY] > 0 && this.allowedStorageUse) {
            if (this.creep.withdraw(this.room.storage, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                this.creep.moveTo(this.room.storage);
            }
        } else if (containers.length) {
            const maxContainer = _.max(containers, c => c.store[RESOURCE_ENERGY]);

            if (this.creep.withdraw(maxContainer, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                this.creep.moveTo(maxContainer);
            }
        } else if(this.room.storage && this.room.storage.store[RESOURCE_ENERGY] > 0) {
            if (this.creep.withdraw(this.room.storage, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                this.creep.moveTo(this.room.storage);
            }
        } else {
            console.log("Looking for sources");
            const source = this.creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);

            if (this.creep.harvest(source) === ERR_NOT_IN_RANGE) this.creep.moveTo(source);
        }
    }

    static createCreep(spawn, name, memory, body) {
        if (spawn) {
            const creepBody = body ? body : [WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE];
            memory.homeRoom = spawn.room.name;

            const status = spawn.spawnCreep(creepBody, name, { memory });

            if (status === OK) console.log(`A new ${memory.role} has been spawned`);
        }
    }

    static createCreepBody(energy) {
        const creepBody = [];

        while (energy > 200) {
            creepBody.push(WORK, CARRY, MOVE);
            energy -= 200;
        }

        return creepBody;
    }
}

module.exports = Creep;
