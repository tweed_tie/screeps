const Creep = require('components_creeps_creep');

class DistanceHarvester extends Creep {
    constructor(creep) {
        super(creep);

        this.homeRoom = this.memory.homeRoom;
        this.targetRoom = this.memory.targetRoom;
    }

    run() {
        this.updateWorkingStatus();

        if (this.memory.working) {
            if (this.room.name === this.targetRoom) {
                const source = this.creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
                if (source) {
                    if (this.creep.harvest(source) === ERR_NOT_IN_RANGE) {
                        this.creep.moveTo(source);
                    }
                }
            } else {
                const exit = this.room.findExitTo(this.targetRoom);
                this.creep.moveTo(this.creep.pos.findClosestByRange(exit));
            }
        } else {
            if (this.room.name === this.homeRoom) {
                const storage = this.room.storage;
                if (storage) {
                    if (this.creep.transfer(storage, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                        this.creep.moveTo(storage);
                    }
                }
            } else {
                const exit = this.room.findExitTo(this.homeRoom);
                this.creep.moveTo(this.creep.pos.findClosestByRange(exit));
            }
        }
    }
}

module.exports = DistanceHarvester;
