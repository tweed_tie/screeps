const Creep = require('components_creeps_creep');

const Upgrader = require('components_creeps_roles_upgrader');

class Harvester extends Creep {
    constructor(creep) {
        super(creep);
    }

    run() {
        this.updateWorkingStatus();

        if (this.memory.working) {
            this.work();
        } else {
            const target = this.creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                filter: (s) => ( s.structureType === STRUCTURE_SPAWN
                                || s.structureType === STRUCTURE_EXTENSION
                                || s.structureType === STRUCTURE_TOWER )
                                && s.energy < s.energyCapacity
            });

            if (target) {
                if (this.creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    this.creep.moveTo(target);
                }
            } else {
                const upgrader = new Upgrader(this.creep);
                upgrader.run();
            }
        }
    }
}

module.exports = Harvester;
