const Creep = require('components_creeps_creep');

class Miner extends Creep {
    constructor(creep) {
        super(creep);

        this.sourceId = this.memory.sourceId;
    }

    run() {
        const source = Game.getObjectById(this.sourceId);
        const container = source.pos.findInRange(FIND_STRUCTURES, 1, {
            filter: s => s.structureType === STRUCTURE_CONTAINER
        })[0];

        if (this.creep.pos.isEqualTo(container.pos)) {
            this.creep.harvest(source);
        } else {
            this.creep.moveTo(container);
        }
    }
}

module.exports = Miner;
