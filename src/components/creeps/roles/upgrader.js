const Creep = require('components_creeps_creep');

class Upgrader extends Creep {
    constructor(creep) {
        super(creep);

        this.allowedStorageUse = true;
    }

    run() {
        this.updateWorkingStatus();

        if (this.memory.working) {
            this.work();
        } else {
            const targets = this.room.find(FIND_MY_STRUCTURES, {
                filter: { structureType: STRUCTURE_CONTROLLER }
            });

            if (targets) {
                if (this.creep.transfer(this.room.controller, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    this.creep.moveTo(this.room.controller);
                }
            }
        }
    }
}

module.exports = Upgrader;
