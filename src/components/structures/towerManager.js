class TowerManager {
    constructor(room, towers) {
        this.room = room;
        this.towers = towers;
    }

    run() {
        for (const tower of this.towers) {
            const hostileTarget = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);

            if (hostileTarget) {
                tower.attack(hostileTarget);
            } /*else if (tower.energy > 500) {
                const target = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (s) => s.hits < s.hitsMax
                });

                tower.repair(target);
            } */
        }
    }
}

module.exports = TowerManager;
