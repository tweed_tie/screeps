const CreepManager = require('components_creeps_creepManager');
const TowerManager = require('components_structures_towerManager');

class RoomManager {
    constructor(room) {
        this.room = room;
    }

    run() {
        const creepsInRoom = this.room.find(FIND_MY_CREEPS);
        const creepManager = new CreepManager(this.room, creepsInRoom);
        creepManager.run();

        const towersInRoom = this.room.find(FIND_STRUCTURES, {
            filter: s => s.structureType === STRUCTURE_TOWER
        });
        const towerManager = new TowerManager(this.room, towersInRoom);
        towerManager.run();
    }
}

module.exports = RoomManager;
